import Plotly from 'plotly.js-dist-min';

// - There must be exactly one entry with `parent` set to `null`; this is the root entry.
// - Every entry except the root entry must have:
//   - a unique label
//   - a parent that matches the label of another entry
let data = [

	{ label: 'You', parent: null, },

	{ label: 'Romance', parent: 'You', },
	{ label: 'Chemistry', parent: 'Romance', },
	{ label: 'Feelings of Love', parent: 'Romance', },

	{ label: 'Friendship', parent: 'You', },
	{ label: 'Companionship', parent: 'Friendship', },
	{ label: 'Playfulness', parent: 'Friendship', },
	{ label: 'Shared Interest', parent: 'Friendship', },
	{ label: 'Shared Activities', parent: 'Friendship', },

	{ label: 'Domestic', parent: 'You', },
	{ label: 'Sharing a Dwelling', parent: 'Domestic', },
	{ label: 'Sharing Chores', parent: 'Domestic', },

	{ label: 'Sexual', parent: 'You', },
	{ label: 'Arousal', parent: 'Sexual', },
	{ label: 'Physical Intimacy', parent: 'Sexual', },

	{ label: 'Physical Touch', parent: 'You', },
	{ label: 'Dance', parent: 'Physical Touch', },
	{ label: 'Cuddles', parent: 'Physical Touch', },
	{ label: 'Hugs', parent: 'Physical Touch', },
	{ label: 'Pets', parent: 'Physical Touch', },
	{ label: 'Holding Hands', parent: 'Physical Touch', },
	{ label: 'Massage', parent: 'Physical Touch', },

	{ label: 'Life Partner', parent: 'You', },
	{ label: 'Sharing Goals', parent: 'Life Partner', },
	{ label: 'Embracing Change', parent: 'Life Partner', },

	{ label: 'Caregiver', parent: 'You', },
	{ label: 'Giving Care', parent: 'Caregiver', },
	{ label: 'Receiving Care', parent: 'Caregiver', },

	{ label: 'Co-Caregiver', parent: 'You', },
	{ label: 'Childcare', parent: 'Co-Caregiver', },
	{ label: 'Animal Care', parent: 'Co-Caregiver', },
	{ label: 'Plant Care', parent: 'Co-Caregiver', },
	{ label: 'Family Care', parent: 'Co-Caregiver', },

	{ label: 'Emotional Intimacy', parent: 'You', },
	{ label: 'Sharing', parent: 'Emotional Intimacy', },
	{ label: 'Vulnerability', parent: 'Emotional Intimacy', },
	{ label: 'Receptivity', parent: 'Emotional Intimacy', },

	{ label: 'Emotional Support', parent: 'You', },
	{ label: 'Listening', parent: 'Emotional Support', },
	{ label: 'Consultation', parent: 'Emotional Support', },
	{ label: 'Confidence', parent: 'Emotional Support', },

	{ label: 'Social Partners', parent: 'You', },
	{ label: 'Seen Together', parent: 'Social Partners', },
	{ label: 'Events', parent: 'Social Partners', },
	{ label: 'Friends', parent: 'Social Partners', },
	{ label: 'Family', parent: 'Social Partners', },
	{ label: 'Work', parent: 'Social Partners', },
	{ label: 'Social Media', parent: 'Social Partners', },

	{ label: 'Financial Sharing', parent: 'You', },
	{ label: 'Money', parent: 'Financial Sharing', },
	{ label: 'Accounts', parent: 'Financial Sharing', },
	{ label: 'Payments', parent: 'Financial Sharing', },
	{ label: 'Property', parent: 'Financial Sharing', },

	{ label: 'Kink', parent: 'You', },
	{ label: 'Fantasies', parent: 'Kink', },
	{ label: 'Masochism', parent: 'Kink', },
	{ label: 'Roles', parent: 'Kink', },
	{ label: 'Sadism', parent: 'Kink', },

	{ label: 'Power Dynamic', parent: 'You', },
	{ label: 'Age Difference', parent: 'Power Dynamic', },
	{ label: 'Pet Play', parent: 'Power Dynamic', },
	{ label: 'Physical Size', parent: 'Power Dynamic', },
	{ label: 'Teacher / Student', parent: 'Power Dynamic', },
	{ label: 'Boss / Employee', parent: 'Power Dynamic', },
	{ label: 'Dom / sub', parent: 'Power Dynamic', },
	{ label: 'Top / Bottom', parent: 'Power Dynamic', },

	{ label: 'Collaborative Partners', parent: 'You', },
	{ label: 'Teaching', parent: 'Collaborative Partners', },
	{ label: 'Projects', parent: 'Collaborative Partners', },
	{ label: 'Art', parent: 'Collaborative Partners', },
	{ label: 'Organization', parent: 'Collaborative Partners', },
	{ label: 'Music', parent: 'Collaborative Partners', },
	{ label: 'Sports', parent: 'Collaborative Partners', },
	{ label: 'Hobbies', parent: 'Collaborative Partners', },

	{ label: 'Buisness Partners', parent: 'You', },
	{ label: 'Collaboration', parent: 'Buisness Partners', },
	{ label: 'Social', parent: 'Buisness Partners', },
	{ label: 'Financial', parent: 'Buisness Partners', },

	{ label: 'Love Languages', parent: 'You', },
	{ label: 'Acts of Service', parent: 'Love Languages', },
	{ label: 'Words of Affirmation', parent: 'Love Languages', },
	{ label: 'Gifts', parent: 'Love Languages', },
	{ label: 'Quality Time', parent: 'Love Languages', },
	{ label: 'Touch', parent: 'Love Languages', },

// Reverse the data so it follows the order of the colors, clockwise from 3'oclock around.
].reverse();

// List branches, a.k.a. the list of unique parents
data.branches = {};
data.map((row) => {
	data.branches[row.parent] = true;
	return row;
});

// List leaves
data.is_leaf = (row) => data.branches[row.label] !== true;
data.leaves = data.map((row) => {
	if(data.is_leaf(row)) return row.label;
}).flat();

// Set values
data.map((row) => { 
	row.value = data.is_leaf(row) ? 1.0 : 0.0;
	return row;
});

let charts = [{
	type: 'sunburst',

	labels: data.map((row) => row.label),
	parents: data.map((row) => row.parent),
	values: data.map((row) => row.value),

	sort: false,

	branchvalues: 'remainder',

	textposition: 'inside',
	texttemplate: '    %{label}    ', // This is a dumb hack because we can't pad sectors.

	insidetextorientation: 'radial', // auto, horizontal, radial, or tangential

	textfont: {
		size: 16,
		family: "Fira Sans Bold",
	},

	leaf: {
		opacity: 0.65,
	},

	marker: {
		line: {
			width: 2,
		},
	},

	config: {
		displayModeBar: false,
		editable: false,
	},
}];

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  l       The lightness
 * @return  Array           The RGB representation
 */
function hslToRgb(h, s, l) {
  var r, g, b;

  if (s == 0) {
    r = g = b = l; // achromatic
  } else {
    function hue2rgb(p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1/6) return p + (q - p) * 6 * t;
      if (t < 1/2) return q;
      if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;

    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }

  return [ r * 255, g * 255, b * 255 ];
}

let hslToHex = (h, s, l) => {
	let rgb = hslToRgb(h, s, l).map((n) => Math.round(n).toString(16));
	return `#${rgb[0]}${rgb[1]}${rgb[2]}`;
};

let colorway = (n) => {
	let colors = [];
	let step = 1.0 / n;
	for(let i = 0; i < n; i++) {
		colors.push(hslToHex(i * step, 0.65, 0.75));
	}
	return colors;
};

let layout = (id) => {
	let element = document.getElementById(id);
	if(!element) {
		console.log(`Unable to find element with id: '${id}'`)
		return;
	}

	return {
		margin: {
			autoexpand: false,
			pad: 0,
			t: 0,
			r: 0,
			b: 0,
			l: 0,
		},
		sunburstcolorway: colorway(16).reverse(), // sigh, by default the colors go anti-clockwise, unlike the labels.
		extendsunburstcolors: false,
		autosize: false,
		width: element.clientWidth,
		height: element.clientHeight,
	};
};

let plot = () => Plotly.newPlot('chart', charts, layout('chart'), {});
addEventListener("resize", (event) => plot());
plot();

